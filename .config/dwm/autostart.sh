dwmblocks &
wal -i ~/pix/wallpapers &
setxkbmap -option caps:swapescape &
pulseaudio &
sh ~/walparser.sh &
xrdb -merge ~/.Xresources &
picom --experimental-backends --transparent-clipping &

