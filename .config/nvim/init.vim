let mapleader =","
source ~/doc/notetake.vim
call plug#begin() " Begin Vim Plug plugins
Plug 'junegunn/limelight.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'} " COC - Completion for code
Plug 'digitaltoad/vim-pug' " Vim Pug - HTML5 syntax plugin
Plug 'junegunn/goyo.vim'
Plug 'mattn/emmet-vim' " Emmet Vim - Emmet for vim
Plug 'PotatoesMaster/i3-vim-syntax'   "I3 Vim Syntax I3 config syntax highlighting
Plug 'autozimu/LanguageClient-neovim', { 
    \ 'branch': 'next',
    \ 'do': './install.sh'
    \ } " LanguageClient Neovim - LSP support for vim
Plug 'neovimhaskell/haskell-vim' " Haskell-Vim - Haskell stuff 
"Utilities
Plug 'scrooloose/nerdtree' " Nerdtree - A file manager for vim
Plug 'ap/vim-css-color' " Vim CSS Color - Coloring for color codes 
"Theming 
Plug 'dracula/vim', { 'as': 'dracula' } " Dracula - Dracula colorscheme 
Plug 'vim-airline/vim-airline' " Vim Airline - Statusline for vim
Plug 'vimwiki/vimwiki'	
Plug 'jceb/vim-orgmode'

call plug#end() " End Vim Plug plugins


let g:coc_user_config = {} " Something to do with coc config, I forget.
colorscheme dracula " Use Dracula as the colorscheme
let g:airline_powerline_fonts = 1 " Use powerline with airline
let g:python_host_prog = '/usr/bin/python' " Python binary
let g:python3_host_prog = '/usr/bin/python3' " Python3 binary
set noswapfile " Get rid of swapfile 
syntax on " Enable syntax highlighting
set number " Enable line numbers 
set termguicolors " Enable termguicolors
set t_Co=256 " Enable 256 Color 
set nocompatible
filetype plugin on

"Mappings for navigating splits
nnoremap <C-h> <C-w>h 
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
"Spellcheck - control with keybindings
noremap \sd :set nospell<CR>
noremap \se :set spell spelllang=en_us<CR>
noremap ,c :w! \| :silent exec '!pdflatex % '<CR>
noremap ,z :silent exec '!zathura %:r.pdf &'<CR>
noremap ,n :Goyo<CR>

"Mappings for resizing splits
noremap <silent> <C-Right> :vertical resize +3<CR>
noremap <silent> <C-Left> :vertical resize -3<CR>
noremap <silent> <C-Down> :resize +3<CR>
noremap <silent> <C-Up> :resize -3<CR>
	
