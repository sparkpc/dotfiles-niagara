#Prompt

PS1="%B%F{red}[%f%b%B%F{red}%n%f%b%B%F{33}@%f%b%B%F{green}%m%f%b %B%F{yellow}%~%f%b%B%F{yellow}]%f%b%B%F{6}$>%f%b "
# function powerline_precmd() {
#function powerline_precmd() {
(cat ~/.cache/wal/sequences)
#COMPLETION
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
#Vi
bindkey -v
export KEYTIMEOUT=1
zstyle ":completion:*:commands" rehash 1

#History
HISTSIZE=10000000
SAVEHIST=10000000
setopt INC_APPEND_HISTORY_TIME

#Opts 
setopt autocd
#Exports
export VISUAL=nvim;
export EDITOR=nvim;
#Aliases
source ~/.zsh/alias.zsh
alias nc='nvim ~/.config/nvim/init.vim'
alias dwme='nvim ~/wm/dwm/config.h'
alias dwmcd='cd ~/wm/dwm/'
alias dwmb='cd ~/wm/dwm/ && sudo make install && make clean && cd -'
alias zshe='nvim ~/.zshrc'
alias s='sudo systemctl'
alias zc='nvim ~/.zshrc'
alias kx='killall xinit'
alias sx='startx'
alias c="git clone"
alias C="cd ~/.config"
alias ys="youtube-dl --extract-audio --audio-format flac"
#Only use if you want exa, as an ls replacement.
#alias ls='exa -1'
alias lss='exa -la | grep'
autoload -U compinit promptinit
compinit
source /home/jack/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


